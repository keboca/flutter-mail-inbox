import 'package:flutter/material.dart';
import 'package:imap_client/imap_client.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KEBOCA Mail',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'KEBOCA Inbox'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _subjects = StorageMail.loadEmail();
  var isLoading = false;

  Widget _buildRow(String subject) {
    return ListTile(
      title: Text(
        subject,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
        ),
      ),
      trailing: Icon(
        Icons.open_in_new,
        color: Colors.tealAccent,
      ),
    );
  }

  Widget _buildSubjectsList() {
    return ListView.builder(
        itemCount: _subjects.length * 2,
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) {
            return Divider();
          }

          final index = i ~/ 2;
          if (_subjects.isEmpty || index >= _subjects.length) {
            return null;
          }

          return _buildRow(_subjects[index]);
        });
  }

  void _consumeEmails() {
    //
    ConsumeMail service = new ConsumeMail();
    service.consumeMail().then((result) => setState(() {
          result.forEach((key, email) => _subjects.add(email.subject));
          isLoading = false;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: isLoading
          ? Center(child: CircularProgressIndicator())
          : _buildSubjectsList(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            isLoading = true;
          });
          _consumeEmails();
        },
        tooltip: 'Sync',
        child: Icon(Icons.sync),
      ),
    );
  }
}

class Email {
  final String subject;
  final String body;

  Email({this.subject, this.body});

  factory Email.fromResult(Map<String, dynamic> result) {
    return Email(
      subject: result.values.first.toString(),
      body: result.values.last.toString(),
    );
  }
}

class ConsumeMail {
  ImapClient client = new ImapClient();
  Map<int, Email> emails = Map<int, Email>();

  Future<dynamic> consumeMail() async {
    emails.clear();

    await client.connect("imap.hostname.com", 993, true);
    await client
        .authenticate(new ImapPlainAuth("username", "password"));

    ImapFolder inbox = await client.getFolder("inbox");
    for (var i = inbox.mailCount; i > 0; i--) {
      //
      await inbox.fetch([
        "BODY.PEEK[HEADER.FIELDS (SUBJECT)]",
        "RFC822.TEXT",
      ], messageIds: [
        i
      ]).then((Map<int, Map<String, dynamic>> result) =>
          emails[i] = Email.fromResult(result[i]));
    }

    await client.logout();
    return emails;
  }
}

class StorageMail {
  static List<String> loadEmail() {
    return <String>[
      //@TODO Load information stored previously... Perhaps from any local storage?
    ];
  }
}
